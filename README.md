# Reacty

Minimal reactive functional library for python and micropython

## Example

```python
import reacty as rx
import random ar rnd

# initialize a simple processing pipeline
#
# emit signal 1s     map random to the stream    average 10 values    print  the result
rx.Timer(1000)    >> rx.Map(rnd.random)       >> rx.AverageN(10)   >> rx.Print()

# emit signal .5s    map random to the stream    filter out value < 0.5                  avearage 10 values     print results
rx.timer(500)     >> rx.Map(rnd.random)       >> rx.Filter(lambda x : return x > 0.5) >> rx.Average(10)      >> rx.Print() 

# execute the pipeline
rx.loop()

```

