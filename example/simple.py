"""
Simple example
"""
import random as rnd
import reacty as rx

# initialize a simple processing pipeline
#
# emit signal .1s     map random to the stream   average 10 values    print  the result
_ = rx.Timer(100)     >> rx.Map(rnd.random)       >> rx.AverageN(10)   >> rx.Print()

# emit signal .05s    map random to the stream    filter out value < 0.5             avearage 10 values  print results
_ = rx.Timer(50)      >> rx.Map(rnd.random)        >> rx.Filter(lambda x : (x > 0.5)) >> rx.AverageN(10)     >> rx.Print() 

# execute the pipeline
rx.loop()
