"""
Simple reactive library for python and micropython for non multi-threading enviroments.
author: Martino G. Ferrari <manda.mgf@gmail.com>
"""

import sys

# setup basic functions for different os
if sys.platform == 'esp8266':
    # For micropython boards
    import utime
    def __now__():
        return utime.ticks_ms()

    def __wait__(millis):
        utime.sleep_ms(millis)

    def __deep_wait__(millis):
        # TODO: call the deep sleep upython function
        __wait__(millis)
else:
    # for desktop use
    import time
    def __now__():
        return int(round(time.time() * 1000))

    def __wait__(millis):
        time.sleep(millis/1000.0)

    def __deep_wait__(millis):
        # for normal computers there is no
        # need of implementing a deep sleep wait
        __wait__(millis)


class Manager:
    """Manage the streams."""
    blocks = []


class Type:
    """Value Type constants.
    As enums are not supported by micropython the class does not
    inherits from Enum class."""
    v_none = -1
    v_any = 0
    v_int = 1
    v_bool = 2
    v_float = 3
    v_str = 4
    v_list = 5
    v_number = 6
    numbers = set([v_float, v_int])


def compatible(type_x, type_y):
    """Check if two types are compatibles."""
    if type_x == type_y:
        return True
    if type_y == Type.v_any or type_x == Type.v_any:
        return True
    if type_x == Type.v_number and type_y in Type.numbers:
        return True
    if type_x == Type.v_number and type_y in Type.numbers:
        return True
    return False


def loop(delay=10000, deepsleep=False):
    """
    Main loop function.
    When this functions is called all the streams are initilized
    and the execution of the processing pipeline start

    Parameters:
    -----------
    - dalay: maximum delay between two iterations of the main loop (in ms, default 10000)
    - deepsleep: enable/disable deep sleep between iterations to save battery (default false)
    """
    while True:
        next_in = delay
        for strm in Manager.blocks:
            if strm.ready():
                try:
                    strm.trig()
                except Exception as exception:
                    # log any kind of exception
                    loggy.log_err("id", getattr(exception, 'message', repr(exception)))
                next_in = 0
                break
            else:
                expected = strm.next_in()
                if expected >= 0:
                    next_in = next_in if next_in < expected else expected
        if next_in > 0:
            if deepsleep:
                __deep_wait__(next_in)
            else:
                __wait__(next_in)


class Message:
    """Simple message struct."""
    def __init__(self, sender, reciver, value):
        self.time = __now__()
        self.sender = sender
        self.reciver = reciver
        self.value = value


class ProcessingBlock:
    """
    Generic procssing block class.
    ProcessingBlock is the basic object of reacty
    functional processing pipeline. It represent an
    atomic operation or function. it takes (or not)
    an input and generate (or not) an output.

    For recieving the inputs from the previous streams and
    transmitting the results to the next streams it use the
    `Message` struct.

    Simply you can see a stream as a processing block:
    ```
    Message(input) => Stream => Message(output)
    ````

    Parameters
    ----------
    - in_type: input type required from this stream
    - out_type: output type generated from this stream
    """
    def __init__(self, in_type, out_type):
        Manager.blocks.append(self)
        self.in_type = in_type
        self.out_type = out_type
        self.__messages__ = []
        self.connections = []

    def ready(self):
        """Check if stream is ready to compute the output."""
        return len(self.__messages__) > 0

    def next_in(self):
        """
        Computes the expected delay befor the next events.

        Returns
        -------
        Expected delay in ms or -1 if event is not predictable.
        """
        return -1

    def connect(self, next_block):
        """
        Connect two block togheter and so create a stream. and perform
        simple compatibility check by comparing the input of
        the following block with the output of the current block.

        Parameters:
        -----------
        - next: block to connect with current one.

        Returns
        -------
        The next block if the two are compatible else `None`.
        In the future raise an error instead.
        """
        if compatible(self.out_type, next_block.in_type):
            self.connections.append(next_block)
            return next_block
        # TODO raise error instead
        return None

    def trig(self):
        """Trig the computation of the next output (if any)."""

    def message(self, msg):
        """
        Stores an incoming message in the message queque.

        Parameters:
        -----------
        - msg: incoming message
        """
        self.__messages__.append(msg)

    def __dispatch_value__(self, val):
        """
        Internal methods: sends the computed value to all connected blocks.
        """
        for obs in self.connections:
            msg = Message(self, obs, val)
            obs.message(msg)

    def __consume_msg__(self):
        """
        Internal method: removes an used message.
        """
        if len(self.__messages__) > 0:
            return self.__messages__.pop(0)
        return None

    def __rshift__(self, other):
        """
        Syntactic sugar
        ---------------
        Use `>>` to connect blocks:
        ```
        block(A) >> Stream(B) = Stream(A).connect(Stream(B))
        ```
        """
        return self.connect(other)


class InputBlock(ProcessingBlock):
    """
    Represents a block that does not take any input but only
    generate output. Some examples of `InputBlock` are timers,
    hardware inputs e altri readers.

    Parameters:
    -----------
    - out_type: output type.
    """
    def __init__(self, out_type):
        ProcessingBlock.__init__(self, Type.v_none, out_type)

class OutputBlock(ProcessingBlock):
    """
    """



class Map(ProcessingBlock):
    """
    Map any function to a stream.
    For example if you want to use the `cos` function inside a processing pipeline:
    ```python
    ... Block(A) >> Map(cos, Type.v_number, Type.v_float) >> Block(B) ...
    ```


    Parameters:
    -----------
    - fn: custom function
    - in_type: input type required (default: any)
    - out_type: output type (default: any)
    - payload: persistent data required by the function (default: `None`)
    """
    def __init__(self, fn, in_type=Type.v_any, out_type=Type.v_any, payload=None):
        ProcessingBlock.__init__(self, in_type, out_type)
        self.callback = fn
        self.payload = payload

    def trig(self):
        """
        Execute mapped function on the input message.

        Returns
        -------
        if it was possible to execute the function true else false.
        """
        msg = self.__consume_msg__()
        if msg:
            value = msg.value
            if value is not None and self.payload is not None:
                res = self.callback(value, self.payload)
            elif value is not None:
                res = self.callback(value)
            elif self.payload is not None:
                res = self.callback(self.payload)
            else:
                res = self.callback()
            self.__dispatch_value__(res)
            return True
        return False


class Timer(ProcessingBlock):
    """Simple timer source stream."""
    def __init__(self, delay_ms):
        ProcessingBlock.__init__(self, Type.v_none, Type.v_any)
        self.delay = delay_ms
        self.last = __now__()

    def next_in(self):
        """Compute when the next timer event will heppen"""
        return self.delay - (__now__() - self.last)

    def ready(self):
        """Check if enough time is passed."""
        return (__now__() - self.last) >= self.delay

    def trig(self):
        """Trig a void event."""
        self.__dispatch_value__(None)
        self.last = __now__()
        return True


class BufferN(ProcessingBlock):
    """Store N values before sending to next stream"""
    def __init__(self, N):
        ProcessingBlock.__init__(self, Type.v_any, Type.v_list)
        self.__n_values__ = N

    def ready(self):
        return len(self.__messages__) >= self.__n_values__

    def trig(self):
        if len(self.__messages__) < self.__n_values__:
            return False
        buffer = [x.value for x in self.__messages__]
        self.__messages__ = []
        self.__dispatch_value__(buffer)
        return True


class BufferT(ProcessingBlock):
    """bufferize values for T ms before sending to next stream."""
    def __init__(self, delta_ms):
        ProcessingBlock.__init__(self, Type.v_any, Type.v_list)
        ProcessingBlock.delay = delta_ms
        ProcessingBlock.last = __now__()

    def ready(self):
        return (__now__() - self.last) >= self.delay

    def trig(self):
        buffer = [x.value for x in self.__messages__]
        self.__messages__ = []
        self.last = __now__()
        self.__dispatch_value__(buffer)
        return True


class Const(ProcessingBlock):
    """Emit a constant value every time an incoming message is recieved."""
    def __init__(self, const, vtype=Type.v_any):
        ProcessingBlock.__init__(self, Type.v_any, vtype)
        self.__const__ = const

    def trig(self):
        self.__consume_msg__()
        self.__dispatch_value__(self.__const__)
        return True


class Count(ProcessingBlock):
    """Count the number of incoming messages recieved."""
    def __init__(self):
        ProcessingBlock.__init__(self, Type.v_any, Type.v_int)
        self.__acc__ = 0

    def trig(self):
        msg = self.__consume_msg__()
        if msg:
            self.__acc__ += 1
            self.__dispatch_value__(self.__acc__)
            return True
        return False


class Accumulate(ProcessingBlock):
    """Accumulate the incoming messages."""
    def __init__(self):
        ProcessingBlock.__init__(self, Type.v_number, Type.v_float)
        self.__acc__ = 0.0

    def trig(self):
        msg = self.__consume_msg__()
        if msg:
            self.__acc__ += msg.value
            self.__dispatch_value__(self.__acc__)
            return True
        return False


class AccumulateN(ProcessingBlock):
    """Accumulate N incoming messages."""
    def __init__(self, N):
        ProcessingBlock.__init__(self, Type.v_number, Type.v_float)
        self.__acc__ = 0.0
        self.__n_values__ = N

    def ready(self):
        return len(self.__messages__) >= self.__n_values__

    def trig(self):
        if len(self.__messages__) < self.__n_values__:
            return False
        acc = 0
        for msg in self.__messages__:
            acc += msg.value
        self.__messages__ = []
        self.__dispatch_value__(acc)
        return True


class AccumulateT(ProcessingBlock):
    """Accumulate incoming messages for T ms."""
    def __init__(self, delta_ms):
        ProcessingBlock.__init__(self, Type.v_any, Type.v_list)
        ProcessingBlock.delay = delta_ms
        ProcessingBlock.last = __now__()

    def ready(self):
        return (__now__() - self.last) >= self.delay

    def trig(self):
        acc = 0
        for msg in self.__messages__:
            acc += msg.value
        self.__messages__ = []
        self.last = __now__()
        self.__dispatch_value__(acc)
        return True


class AverageN(ProcessingBlock):
    """Average N values."""
    def __init__(self, N):
        ProcessingBlock.__init__(self, Type.v_number, Type.v_float)
        self.__acc__ = 0.0
        self.__n_values__ = N

    def ready(self):
        return len(self.__messages__) >= self.__n_values__

    def trig(self):
        if len(self.__messages__) < self.__n_values__:
            return False
        acc = 0
        for msg in self.__messages__:
            acc += msg.value
        n_values = len(self.__messages__)
        self.__messages__ = []
        self.__dispatch_value__(acc/n_values)
        return True


class AverageT(ProcessingBlock):
    """Average values accumulated for T ms."""
    def __init__(self, delta_ms):
        ProcessingBlock.__init__(self, Type.v_any, Type.v_list)
        ProcessingBlock.delay = delta_ms
        ProcessingBlock.last = __now__()

    def ready(self):
        return (__now__() - self.last) >= self.delay

    def trig(self):
        acc = 0
        for msg in self.__messages__:
            acc += msg.value
        n_values = len(self.__messages__)
        self.__messages__ = []
        self.last = __now__()
        self.__dispatch_value__(acc/n_values)
        return True


class Filter(ProcessingBlock):
    """Pass the value to next stream only if it verify some conditions (defined by filter_fn)."""
    def __init__(self, filter_fn, payload=None):
        ProcessingBlock.__init__(self, Type.v_any, Type.v_any)
        self.__filter_fn__ = filter_fn
        self.__payload__ = payload

    def trig(self):
        msg = self.__consume_msg__()
        if msg:
            val = msg.value
            res = self.__filter_fn__(val, self.__payload__) if self.__payload__\
                    else self.__filter_fn__(val)
            if res:
                self.__dispatch_value__(val)
                return True
        return False


class Print(ProcessingBlock):
    """Print the recieved value and pass it to the next."""
    def __init__(self):
        ProcessingBlock.__init__(self, Type.v_any, Type.v_any)

    def trig(self):
        msg = self.__consume_msg__()
        if msg:
            val = msg.value
            print(val)
            self.__dispatch_value__(val)
            return True
        return False


if __name__ == '__main__':
    ## A simple example....
    ## Start two parallel processes one counting the ticks
    ## One accumulating values for 5s

    # every 0.5s  emit 1      accumulate for 5s    print the accumulated value
    _ = Timer(500) >> Const(1) >> AccumulateT(5000) >> LoggY(message='value: ', log_id='Stream 2')
    # every 1s     count it   print the accumulated value
    _ = Timer(1000) >> Count() >> LoggY(message='value: ', log_id='Stream 1')
    loop()
